const express = require('express');
const bodyParser = require('body-parser');

let app = express();
const port = process.env.PORT || 3000;


app.listen(port, () =>{
    console.log(`Started up at port ${port}`);
});
module.exports = {app};