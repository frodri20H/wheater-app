import express from 'express';
import path from 'path';
import mongoose from 'mongoose';
import hbs from 'express-handlebars';
import bodyParser from 'body-parser';

mongoose
    .connect('mongodb://mongodb/wheaterapp')
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err));

// Loading Models
require('./models/User');

// Loading routes
import routes from './routes/routes';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(express.static(path.join(__dirname, '../public')));
app.use('/public', express.static('public'));

// Handlebars MiddleWare
app.engine(
  '.hbs',
  hbs({
    helpers: {
      section(name, options) {
        if (!this._sections) this._sections = {};
        this._sections[name] = options.fn(this);
        return null;
      },
    },
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: path.join(__dirname, 'views/layouts'),
    partialsDir: path.join(__dirname, 'views/partials'),
  }),
);
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));

app.use('/', routes);

module.exports = app;