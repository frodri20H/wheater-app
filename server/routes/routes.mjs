import express from 'express';

const router = express.Router();

import authController from '../controllers/authController.mjs'; // Controller authenticate.
import weatherController from '../controllers/weatherController'; // Controller App
import userController from '../controllers/userController'; // Controller user

import { authenticate } from '../middleware/authenticate.mjs';

// Index App route
router.get('/', authenticate, weatherController.index);

// Login related routes
router.get('/login', authController.loginForm);
router.post('/login', authController.login);
router.post('/createuser', userController.createUser);
router.get('/logout', authenticate, authController.logout);

module.exports = router;