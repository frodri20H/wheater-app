import Search from './models/Search';
import * as searchView from './views/searchView.mjs';
import { elements } from './views/dom.mjs';

/** Global state of app
 * - Search objetc
 * - Current recipe object
 * - Save searchs 
 */

const state = {};

const controlSearch = async () => {
    //1) Get query from view
    const query = searchView.getInput();

    try {
        if (query) {
            // 2) New search objetc and add state
            state.data = new Search(query);
            
            // 3) Prepare UI for results
            searchView.clearInput();
            searchView.clearResults();

            // 4) Search for result
            await state.data.getResult();
            console.log(state.data);
            console.log(state.data.address, state.data.query, state.data.temperature, state.data.apparentTemperature); 

            // 5) Render result on UI
            searchView.renderResult(state.data); //Controlar que no renderice si no hay datos.
            
            
        }
    } catch (error) {
        console.log(error)
    };
};

elements.searchForm.addEventListener('submit', e => {
    e.preventDefault();
    controlSearch();
});

