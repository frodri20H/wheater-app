import { elements } from './dom.mjs';

export const getInput = () => {
    return elements.searchInput.value;
};

export const clearInput = () => {
    elements.searchInput.value= '';
};

export const renderResult = data => {
    const markup = `
        <li>
            <p><strong>${data.address}:</strong> It's currently ${data.temperature}º. It feels like  ${data.apparentTemperature}º</p>
        </li>
        `;
        elements.searchResList.insertAdjacentHTML('beforeend', markup);
};

export const clearResults = () => {
    elements.searchResList.innerHTML = '';
}