import mongoose from 'mongoose';

const Sdata = mongoose.model('Sdata', {
    query: {
        type: String
    },
    address: {
        type: String
    },
    temperature: {
        type: String
    },
    apparentTemperature: {
        type: String
    },
    timestamp: {
        createdAt: 'created_at'
    }
});

export {Sdata};