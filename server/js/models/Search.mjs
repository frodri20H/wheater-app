import axios from 'axios';

export default class Search {
    constructor(query) {
        this.query = query,
        this.address,
        this.temperature,
        this.apparentTemperature
    }

    async getResult() {
        const encodedAddress = encodeURIComponent(this.query);
        const proxy = "https://cors-anywhere.herokuapp.com/";
        const geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

        try {
            const geo = await axios.get(geocodeUrl);
            this.address = geo.data.results[0].formatted_address;
            try {
                const response = await axios.get(geocodeUrl);
                if (response.data.status === 'ZERO_RESULTS') {
                    return new Error('Unable to find that address.');
                };
                const lat = response.data.results[0].geometry.location.lat;
                const lng = response.data.results[0].geometry.location.lng;
                console.log(response.data.results[0].formatted_address);
                const weatherUrl = `${proxy}https://api.forecast.io/forecast/4a04d1c42fd9d32c97a2c291a32d5e2d/${lat},${lng}?units=si`;
                const weather = await axios.get(weatherUrl);
                try {
                    this.temperature = weather.data.currently.temperature;
                    this.apparentTemperature = weather.data.currently.apparentTemperature;
                    console.log(`It's currently ${this.temperature}. It feels like ${this.apparentTemperature}.`);
                } catch (e) {
                    if (e.code === 'ENOTFOUND') {
                        console.log('Unable to connect to API servers.');
                    } else {
                        console.log(e.message);
                    };
                };
            } catch (error) {
                console.log(error);
                alert(error);
            }            
        } catch (error) {
            console.log(error);
            alert(error);
        }
    }
}