const path = require('path');
const HtmlWebpackPlugin = require ('html-webpack-plugin');

module.exports = {
    entry: ['babel-polyfill','./server/js/index.mjs'],
    output: {
        path: path.resolve(__dirname,'public'),
        filename: 'js/bundle.js'
    },
    devServer: {
        contentBase: './public'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './server/index.html'
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_module/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    }
};